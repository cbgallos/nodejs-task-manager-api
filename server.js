const express = require("express");
const pool = require("./db");

const app = express();
const port = process.env.PORT || 3000;

// Get all tasks
app.get("/tasks", async (req, res) => {
  try {
    const [rows] = await pool.query("SELECT * FROM tasks");
    res.json(rows);
  } catch (err) {
    console.error(err);
    res.status(500).send("Server Error");
  }
});

// Add a new task
app.post("/tasks", async (req, res) => {
  const { title, status } = req.body;
  if (!title || !status) {
    return res.status(400).send("Missing required fields (title, status)");
  }

  try {
    const [result] = await pool.query(
      "INSERT INTO tasks (title, status) VALUES (?, ?)",
      [title, status]
    );
    res.json({ id: result.insertId, title, status });
  } catch (err) {
    console.error(err);
    res.status(500).send("Server Error");
  }
});

// Update a task
app.put("/tasks/:id", async (req, res) => {
  const id = req.params.id;
  const { title, status } = req.body;

  try {
    await pool.query("UPDATE tasks SET title = ?, status = ? WHERE id = ?", [
      title,
      status,
      id,
    ]);
    res.json({ message: "Task updated successfully" });
  } catch (err) {
    console.error(err);
    res.status(500).send("Server Error");
  }
});

// Delete a task
app.delete("/tasks/:id", async (req, res) => {
  const id = req.params.id;

  try {
    await pool.query("DELETE FROM tasks WHERE id = ?", [id]);
    res.json({ message: "Task deleted successfully" });
  } catch (err) {
    console.error(err);
    res.status(500).send("Server Error");
  }
});

app.listen(port, () => {
  console.log(`Server listening on port ${port}`);
});
